import kotlinx.coroutines.*
import java.net.HttpURLConnection
import java.net.URL

suspend fun checkWebsite(url: String): Boolean {
    return try {
        val connection = withContext(Dispatchers.IO) {
            URL(url).openConnection()
        } as HttpURLConnection
        connection.responseCode == HttpURLConnection.HTTP_OK
    } catch (e: Exception) {
        false
    }
}

fun main() = runBlocking {
    val websites = listOf(
        "https://www.google.com",
        "https://www.facebook.com",
        "https://www.github.com",
        "https://www.twitter.com",
        "https://www.instagram.com",
        "https://2ch.hk",
        "https://www.reddit.com",
        "https://www.kinopoisk.ru",
        "https://vk.com",
        "https://music.yandex.ru"
    )

    val websiteChecks = websites.map { website ->
        async {
            val availability = checkWebsite(website)
            "Сайт $website ${if (availability) "доступен" else "недоступен"}"
        }
    }

    val results = websiteChecks.awaitAll()

    results.forEach { println(it) }
}